<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nomor 5 function.php</title>
</head>

<body>
    <h1>Hasil Nilai</h1>
    <?php

    function tentukan_nilai($nilai)
    {
        if ($nilai >= 85 && $nilai <= 100) {
            return "Sangat Baik<br>";
        }
        if ($nilai >= 70 && $nilai < 85) {
            return "Baik<br>";
        }
        if ($nilai >= 60 && $nilai < 70) {
            return "Cukup<br>";
        }
        return "Kurang";
    }

    echo tentukan_nilai(98); //Sangat Baik
    echo tentukan_nilai(76); //Baik
    echo tentukan_nilai(67); //Cukup
    echo tentukan_nilai(43); //Kurang


    ?>
</body>

</html>
