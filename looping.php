<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nomor 4 looping.php</title>
</head>

<body>
    <h1>I LOVE PHP</h1>
    <?php
    echo "<h4> ascending <h4>";
    for ($x = 2; $x <= 20; $x += 2) {
        echo $x . " = i love php <br>";
    }

    echo "<h4> descending <h4>";

    for ($x = 20; $x >= 2; $x -= 2) {
        echo $x . " = i love php <br>";
    }
    ?>
</body>

</html>
