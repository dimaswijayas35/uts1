<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nomor 3 array.php</title>
</head>

<body>
    <h1>Kids and Adults</h1>
    <?php
    $AnakAnak = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"];
    $Dewasa = ["Hopper", "Nancy", "Joyce", "Jonathan", "Murray"];

    $banyakAnakAnak = count($AnakAnak);
    $banyakDewasa = count($Dewasa);

    echo 'Kids :';

    foreach ($AnakAnak as $key => $Anak) {
        echo '"' . $Anak . '"';
        if ($key < ($banyakAnakAnak - 1)) {
            echo ", ";
        }
    }
    echo '<br>Adults :';

    foreach ($Dewasa as $key => $Dewasa1) {
        echo '"' . $Dewasa1 . '"'; {
            if ($key < ($banyakDewasa - 1)) {
                echo ", ";
            }
        }
    }

    ?>
</body>

</html>
